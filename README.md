# Instructions
## Install Docker, Docker CLI, and Docker Compose
If you have trouble, see https://docs.docker.com/engine/install/ubuntu/.

```sh
sudo apt-get install docker-ce docker-ce-cli docker-compose-plugin
```

## Run the server
In the project root directory:

```sh
docker compose up
```

# To get the server running (old way)
## Requirements
All PyPi requirements are handled in `requirements.txt`.
1. python >= 3.8
2. django >= 3 (PyPi)
3. channels < 3.0 (PyPi)
4. channels-redis (PyPi)
5. docker & docker.io (package manager)
6. (Recommended) python3-venv

## Clone the repo
```sh
$ git clone ... hand-foot
$ cd hand-foot
```

## Create a python virtual environment
This is optional but *highly* recommended to avoid version conflicts with your other projects.
```sh
# Creates a directory called 'env'
$ python3 -m venv env
```

### Make sure to source the Python environment
```sh
$ source env/bin/activate
```

## Confirm packages with pip
```sh
$ python -m pip install --upgrade pip
$ python -m pip install setuptools wheel
$ python -m pip install -r requirements.txt
```

## Confirm you have the necessary files
Your directory structure should now look something like this:
```sh
$ ls -p
README.md  env/  hf/  requirements.txt  todo.yml  v2/
```

## Running the server
### Start docker with a redis image (for websocket functionality)
```sh
# In a new shell
$ sudo dockerd

# Spin up docker image on port 6379
$ sudo docker run -p 6379:6379 -d redis:5.0
```

### Start the django development server.
```sh
# Activate your Python environment if it's not already
$ source env/bin/activate

# Point `django-admin` to the right interpreter
# This step is only needed if you are using a virtual environment
$ export PYTHONPATH=$(pwd)

# Tell django-admin where to find the project's settings file
$ export DJANGO_SETTINGS_MODULE=v2.settings

# For your first run of the server you'll need to migrate
$ django-admin migrate

# Collect the static files for the server
$ django-admin collectstatic

# Now you can run the server!
$ django-admin runserver
```
