from django.db import models
from django.utils.crypto import get_random_string
from django.contrib.sessions.models import Session
from django.forms.models import model_to_dict

# Create your models here.


class Game(models.Model):
    STATUS_AWAITING = 1
    STATUS_IN_PROGRESS = 2
    STATUS_COMPLETED = 3
    STATUS_CHOICES = [
            (STATUS_AWAITING, 'Awaiting Players'),
            (STATUS_IN_PROGRESS, 'Game in-progress'),
            (STATUS_COMPLETED, 'Completed'),
    ]
    created = models.DateTimeField(auto_now_add=True)
    id = models.CharField(
            primary_key=True,
            default=(lambda: get_random_string(length=8)),
            editable=False,
            max_length=8
    )
    status = models.SmallIntegerField(
            choices=STATUS_CHOICES,
            default=STATUS_AWAITING
    )
    name = models.CharField(
            max_length=256
    )
    draw_pile = models.TextField(default='')
    discard_pile = models.TextField(default='')
    current_turn = models.IntegerField(default=0)
    game_num = models.IntegerField(default=0)

    def __str__(self):
        return self.id + ': ' + self.STATUS_CHOICES[self.status-1][1]

    def to_json(self):
        result = model_to_dict(self)
        result['display_name'] = self.name
        result['name'] = f'games/{self.id}'
        result['draw_pile'] = len(result['draw_pile']) // 2
        result['discard_pile'] = len(result['discard_pile']) // 2

        return result


class Team(models.Model):
    name = models.CharField(
            max_length=64
    )
    game = models.ForeignKey(
            Game,
            on_delete=models.CASCADE,
            related_name='teams'
    )
    current_turn = models.IntegerField(default=0)
    books = models.TextField(default='')
    total_score = models.IntegerField(default=0)


class Player(models.Model):
    session = models.ForeignKey(
            Session,
            on_delete=models.SET_NULL,
            null=True
    )
    game = models.ForeignKey(
            Game,
            on_delete=models.CASCADE,
            related_name='players'
    )
    team = models.ForeignKey(
            Team,
            on_delete=models.SET_NULL,
            null=True,
            related_name='players'
    )
    name = models.CharField(
            max_length=32,
            default="unnamed_player"
    )
    hand = models.TextField(default='')
    foot = models.TextField(default='')

    def to_json(self):
        result = model_to_dict(self)
        result['display_name'] = result['name']
        result['name'] = f'games/{self.game.id}/players/{self.pk}'
        result['foot'] = len(self.foot) > 0
        result['team'] = f'games/{self.game.id}/teams/{self.team.pk}' if self.team else None
        result['game'] = f'games/{self.game.id}'

        return result
