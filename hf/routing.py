from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r'ws/hf/chat/(?P<room_name>\w+)/$', consumers.ChatConsumer.as_asgi()),
    re_path(r'ws/hf/games/(?P<game_id>\w+)/$', consumers.GameDetailConsumer.as_asgi()),
]
