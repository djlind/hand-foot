var chatSocket = new WebSocket(
    'ws://' + window.location.host +
    '/ws' + window.location.pathname);

const PRETTYCARD_VALUE = {
    "A": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "5": "5",
    "6": "6",
    "7": "7",
    "8": "8",
    "9": "9",
    "0": "A",
    "J": "B",
    "Q": "D",
    "K": "E",
    "X": "F",
};
const PRETTYCARD_SUIT = {
    "S": "A",
    "H": "B",
    "D": "C",
    "C": "D",
    "J": "D",
};

function pretty_card(card) {
    if (card.length !== 2) {
        return card;
    }
    let val = PRETTYCARD_VALUE[card[0]];
    let suit = PRETTYCARD_SUIT[card[1]];
    if (val === undefined || suit === undefined) {
        return card;
    }
    let code = "1F0" + suit + val;
    return String.fromCodePoint(parseInt(code, 16));
}

function wrap_pretty(prettycard) {
    return '<span style="font-size:72px">' + prettycard + "</span>";
}

function pretty_cards(cards) {
    let numcards = cards.length / 2;
    let pretty_array = [];
    for (let i = 0; i < numcards; i++) {
        let card = pretty_card(cards.slice(i*2, i*2+2));
        pretty_array.push(card);
    }
    return pretty_array;
}

function get_player_team(data) {
    for (let team of data.teams) {
        if (team.id == data.player.team_id) {
            return team;
        }
    }

    return null;
}

function render_team(team, is_turn, is_game_over) {
    let game_html = `<p>Cumulative Score: ${team.score.toLocaleString()}</p>`;
    if (is_game_over) {
        game_html += `<p>Round Score: ${team.round_score.toLocaleString()}</p>`;
    }
    game_html += `<p>Books Score: ${team.current_score.toLocaleString()}</p>`;

    for (let player of team.players) {
        game_html += `<p>${player.name} 🖐️: ${player.hand_len} cards`;
        if (is_game_over) game_html += ` (${player.scoring.hand.toLocaleString()} points)`
        if (player.has_foot) {
            game_html += ` 🦶`;
            if (is_game_over) game_html += `: (${player.scoring.foot.toLocaleString()} points)`;
        }
        game_html += '</p>';
    }

    let book_ids = {};
    let complete_books = [];
    let incomplete_books = [];
    for (let [rank, book_list] of Object.entries(team.books)) {
        book_list.forEach((book_info, idx) => {
            let book = book_info[0];
            let book_clean_symbol = '💩';
            if (book_info[1]) {
                book_clean_symbol = '✨';
            }
            let book_complete_symbol = '';
            if (book_info[2]) {
                book_complete_symbol = '*';
            }
            let book_is_complete = book_info[2];
            let book_length = book.length / 2;
            let book_pretty = pretty_cards(book).join(" ");
            let book_html = `<li>${book_complete_symbol}${rank} ${book_clean_symbol} book (${book_length}): <span style="font-size: 48px">${book_pretty}</span>`;
            if (is_turn) {
                book_html += ` <input id="book-append-${rank}${idx}" type="text" size="10"/>`;
                book_html += `<input id="book-append-${rank}${idx}-button" type="button" value="Add card"/>`;
                book_ids[`${rank}${idx}`] = book;
            }
            book_html += '</li>';
            if (book_info[2]) {
                complete_books.push(book_html);
            }
            else {
                incomplete_books.push(book_html);
            }
        });
    }
    if (complete_books.length > 0) {
        game_html += '<h3>Complete Books:</h3>';
        game_html += '<ul>' + complete_books.join('') + '</ul>';
    }
    if (incomplete_books.length > 0) {
        game_html += '<h3>Incomplete Books:</h3>';
        game_html += '<ul>' + incomplete_books.join('') + '</ul>';
    }
    return {"out_html": game_html, book_ids};
}

function update_game(data) {
    let game_html = '';
    if (data.player.is_turn) {
        game_html += '<p>It\'s your turn</p>';
    }

    let is_game_over = data.type == "game_end";
    if (is_game_over) {
        let game_element = document.getElementById('game-area');
        game_html += '<h1>GAME OVER!</h1>';
        game_html += '<p>Start a new round? <input id="new-round" type="button" value="Start"/></p>';
        game_html += '<h3>Team Standings:</h3><ul>';
        let sorted_teams = [...data.teams].sort((a, b) => b.score - a.score);
        for (let team of sorted_teams) {
            let total_score = team.score.toLocaleString();
            let round_score = team.round_score.toLocaleString();
            game_html += `<li>${team.name}: ${total_score} (${round_score} this round)</li>`;
        }
        game_html += '</ul>';
    }

    game_html += '<ul style="list-style-type: none; display: grid; grid-template-columns: repeat(auto-fit,minmax(100px, 1fr))">';
    let cards = [];
    for (let i = 0; i < data.player.hand.length / 2; i++) {
        let card = data.player.hand.slice(i*2, i*2+2);
        cards.push(card);
    }
    cards.sort((a, b) => parseInt(PRETTYCARD_VALUE[a[0]], 16) - parseInt(PRETTYCARD_VALUE[b[0]], 16));
    cards.forEach((card, index) => {
        let pretty = wrap_pretty(pretty_card(card));
        game_html += `<li>${pretty}</br>(${card})`;
        if (data.player.is_turn) {
            game_html += `</br><input id="player-card-${index}" type="button" value="Discard"/>`;
        }
        game_html += '</li>';
    });
    game_html += '</ul><h2>Your Team\'s books:</h2>';
    let team = get_player_team(data);
    let {out_html, book_ids} = render_team(team, data.player.is_turn, is_game_over);
    game_html += out_html;
    if (data.player.is_turn) {
        game_html += '<input id="create-new-books" type="text" size="20"/>';
        game_html += '<input id="create-new-books-submit" type="button" value="Submit"/>';
    }
    for (let tm of data.teams) {
        if (tm.id == team.id) {continue};
        game_html += `<h3>${tm.name}</h3>`;
        game_html += render_team(tm, false, is_game_over).out_html;
    }
    game_html += JSON.stringify(data);
    document.getElementById('game-area').innerHTML = game_html;

    // Register card buttons
    if (data.player.is_turn) {
        cards.map(discard_card_button);

        for (let [id, book] of Object.entries(book_ids)) {
            book_append_button(id, book);
        }

        document.querySelector('#create-new-books-submit').onclick = function(e) {
            let booksInputDom = document.querySelector('#create-new-books');
            let booksText = booksInputDom.value;
            chatSocket.send(JSON.stringify({
                'type': 'create_books',
                'books': booksText
            }));

            booksInputDom.value = '';
        };
        document.querySelector('#create-new-books').onkeyup = function(e) {
            if (e.keyCode === 13) {  // enter, return
                document.querySelector('#create-new-books-submit').click();
            }
        };
    }

    // Register new round button
    if (is_game_over) {
        document.getElementById('new-round').onclick = function(e) {
            chatSocket.send(JSON.stringify({
                'type': 'new_round',
            }));
        }
    }
}

function discard_card_button(card, idx) {
    document.getElementById(`player-card-${idx}`).onclick = function(e) {
        chatSocket.send(JSON.stringify({
            'type': 'discard',
            'discard_card': card
        }));
    }
}

function book_append_button(id, book) {
    document.getElementById(`book-append-${id}-button`).onclick = function(e) {
        let bookAppendDom = document.querySelector(`#book-append-${id}`);
        let bookAppendText = bookAppendDom.value;
        chatSocket.send(JSON.stringify({
            'type': 'append_book',
            'book': book,
            'cards': bookAppendText
        }));
    };
    document.getElementById(`book-append-${id}`).onkeyup = function(e) {
        if (e.keyCode === 13) {  // enter, return
            document.querySelector(`#book-append-${id}-button`).click();
        }
    };
}

function update_details(data) {
    player_html = "<h2>Player List:</h2>";

    for (team of data.teams) {
        player_html += '<h3>'
            + `<input id="game-team-join-${team.id}" type="button" value="Join"/>`
            + ' ' + team.name + '</h3>'
            + '<ul>';
        for (player of team.players) {
            player_html += '<li>' + player + '</li>';
        }
        player_html += '</ul>';

    }
    player_html += '<h3>Players not on a team</h3>'
        + '<ul>';
    for (player of data['free_players']) {
        player_html += '<li>' + player + '</li>';
    }
    player_html += '</ul>'; 

    document.getElementById('game-player-list').innerHTML = player_html;

    data.teams.map(join_team_button);
}

function join_team_button(team) {
    document.getElementById(`game-team-join-${team.id}`).onclick = function(e) {
        chatSocket.send(JSON.stringify({
            'type': 'join_team',
            'team_id': team.id
        }));
    }
}

function print_to_chat(message) {
    let chat_log = document.querySelector('#chat-log');
    chat_log.value += (message + '\n');
    chat_log.scrollTop = chat_log.scrollHeight;
}

chatSocket.onmessage = function(e) {
    var data = JSON.parse(e.data);
    if (data['type'] == 'game_details') {
        update_details(data);
    }
    else if (data['type'] == 'chat_message' || data['type'] == 'chat_update') {
        print_to_chat(data['message']);
    }
    else if (data['type'] == 'game_update' || data['type'] == 'game_end') {
        update_game(data);
    }
    else if (data['type'] == 'error') {
        alert(`${data['detail']}`)
    }
};

chatSocket.onclose = function(e) {
    console.error('Chat socket closed unexpectedly');
    print_to_chat('**You were disconnected from the server. Try refreshing.**');
};

chatSocket.onopen = function(e) {
    chatSocket.send(JSON.stringify({
        'type': 'open_connection'
    }));
}

document.querySelector('#chat-message-input').focus();
document.querySelector('#chat-message-input').onkeyup = function(e) {
    if (e.keyCode === 13) {  // enter, return
        document.querySelector('#chat-message-submit').click();
    }
};

document.querySelector('#chat-message-submit').onclick = function(e) {
    var messageInputDom = document.querySelector('#chat-message-input');
    var message = messageInputDom.value;
    chatSocket.send(JSON.stringify({
        'type': 'chat_message',
        'message': message
    }));

    messageInputDom.value = '';
};

document.querySelector('#game-team-submit').onclick = function(e) {
    var messageInputDom = document.querySelector('#game-team-input');
    var team_name = messageInputDom.value;
    chatSocket.send(JSON.stringify({
        'type': 'create_team',
        'name': team_name
    }));
    messageInputDom.value = '';
};

document.querySelector('#game-start').onclick = function(e) {
    chatSocket.send(JSON.stringify({
        'type': 'start_game',
    }));
};
