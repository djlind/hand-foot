from dataclasses import dataclass, asdict
from typing import List

from .game.books import Books
from .game.card import Card
from .game.hand import Hand
from .game.stack import Stack
from .models import Game, Player, Team


REQ_POINTS_FOR_BOOK_CREATION = {
    1: 50,
    2: 90,
    3: 120,
    4: 150,
    5: 120,
    0: 90,
}

DECK = (
    'AH2H3H4H5H6H7H8H9H0HJHQHKH'
    'AD2D3D4D5D6D7D8D9D0DJDQDKD'
    'AC2C3C4C5C6C7C8C9C0CJCQCKC'
    'AS2S3S4S5S6S7S8S9S0SJSQSKS'
    'XJXJ'
)


class InvalidActionError(Exception):
    pass

@dataclass
class PregameTeamState():
    id: int
    name: str
    players: List[str]

@dataclass
class PregameState():
    teams: List[PregameTeamState]
    free_players: List[str]

@dataclass
class PlayerState():
    id: int
    name: str
    has_foot: bool
    hand_len: int

@dataclass
class TeamState():
    id: int
    name: str
    players: List[PlayerState]
    books: Books
    score: int
    current_score: int

    def to_dict(self):
        state = asdict(self)
        state['books'] = self.books.to_dict()
        return state

@dataclass
class GameState():
    teams: List[TeamState]
    discard_top: str
    draw_pile_len: int
    current_turn: int

@dataclass
class PlayerScore():
    id: int
    hand: int
    foot: int

@dataclass
class TeamScore():
    id: int
    player_scores: List[PlayerScore]
    round_score: int

@dataclass
class EndgameState():
    final_state: GameState
    team_scores: List[TeamScore]

@dataclass
class PlayerDetail():
    hand: Hand
    is_turn: bool
    team_id: int


class GameApi:
    game_id: str

    def __init__(self, game_id):
        self.game_id = game_id

    def _get_game(self) -> Game:
        return Game.objects.get(id=self.game_id)

    def _get_current_player(self, game: Game) -> Player:
        current_team = game.teams.all()[game.current_turn]
        return current_team.players.all()[current_team.current_turn]
    
    def get_game_status(self):
        return self._get_game().status

    def get_player(self, session_key: str) -> Player:
        return self._get_game().players.get(session__pk=session_key)
    
    def get_player_detail(self, player: Player) -> PlayerDetail:
        return PlayerDetail(
            hand=Hand.init(player.hand),
            is_turn=player.id == self._get_current_player(self._get_game()).id,
            team_id=player.team.id
        )

    def get_player_score(self, player: Player) -> PlayerScore:
        return PlayerScore(
            id=player.id,
            hand=self._get_stack_score(Stack.init(player.hand)) + (500 if player.hand == '' else 0),
            foot=self._get_stack_score(Stack.init(player.foot))
        )
    
    def _get_stack_score(self, stack: Stack) -> int:
        return -sum(card.score for card in stack)
    
    def get_pregame_state(self) -> PregameState:
        game = self._get_game()
        teams = [
            PregameTeamState(
                id=team.id,
                name=team.name,
                players=[player.name for player in team.players.all()])
            for team in game.teams.all()]
        free_players = [player.name for player in game.players.filter(team__isnull=True)]
        return PregameState(teams=teams, free_players=free_players)
    
    def get_game_state(self) -> GameState:
        game = self._get_game()
        teams = [self._get_team_state(team) for team in game.teams.all()]
        return GameState(
            teams=teams,
            discard_top=str(Stack.init(game.discard_pile).peek()),
            draw_pile_len=Stack.init(game.draw_pile).count,
            current_turn=self._get_current_player(game).id
        )
    
    def _get_team_state(self, team: Team) -> TeamState:
        books = Books.init(team.books)
        return TeamState(
            id=team.id,
            name=team.name,
            players=[self._get_player_state(player) for player in team.players.all()],
            books=books,
            score=team.total_score,
            current_score=books.sum_points()
        )
    
    def _get_player_state(self, player: Player) -> PlayerState:
        return PlayerState(
            id=player.id,
            name=player.name,
            has_foot=player.foot != '',
            hand_len=Stack.init(player.hand).count
        )
    
    def get_endgame_state(self) -> EndgameState:
        game = self._get_game()
        if game.status != Game.STATUS_COMPLETED:
            raise ValueError('cannot get endgame state for game in progress')
        return EndgameState(
            final_state=self.get_game_state(),
            team_scores=[self._get_team_score(team) for team in game.teams.all()]
        )
    
    def _get_team_score(self, team: Team) -> TeamScore:
        player_scores = [self.get_player_score(player) for player in team.players.all()]
        return TeamScore(
            id=team.id,
            player_scores=player_scores,
            round_score=(
                Books.init(team.books).sum_points()
                + sum(score.hand + score.foot for score in player_scores))
        )

    def create_team(self, name: str, player: Player):
        # Create empty team
        game = self._get_game()
        if game.status != Game.STATUS_AWAITING:
            raise ValueError("cannot create team; game has already started")
        team = game.teams.create(name=name)

        self.join_team(team.id, player)

    def join_team(self, team_id, player: Player):
        game = self._get_game()
        if game.status != Game.STATUS_AWAITING:
            raise ValueError("cannot join team; game has already started")
        try:
            team = game.teams.get(id=team_id)
        except Team.DoesNotExist:
            raise ValueError(f"Team with id: {team_id} does not exist")

        # Assign player to team
        player.team = team
        player.save()

    def discard(self, card: str, player: Player):
        game = self._get_game()
        if player.id != self._get_current_player(game).id:
            raise InvalidActionError('it is not your turn')
        
        card_to_discard = Card(card)
        hand = Hand.init(player.hand)
        try:
            hand.take(card_to_discard)
        except ValueError as err:
            raise InvalidActionError(str(err))
        discard = Stack.init(game.discard_pile)
        discard.put([card_to_discard])

        player.hand = str(hand)
        game.discard_pile = str(discard)
        game.save()
        player.save()

        messages = [f'{player.name} discarded {card}']
        self._handle_foot(player, messages)

        if player.hand == '':
            # end the game
            messages.append(f'{player.name} went out, ending the round')
            self._end_game(game)
        else:
            # advance to next player's turn
            self._advance_turn(player.team, game)
            self._start_turn(game, self._get_current_player(game), messages)

        return messages
    
    def _advance_turn(self, team: Team, game: Game):
        team.current_turn = (team.current_turn + 1) % team.players.count()
        game.current_turn = (game.current_turn + 1) % game.teams.count()
        team.save()
        game.save()

    def _end_game(self, game: Game):
        for team in game.teams.all():
            team.total_score += self._get_team_score(team).round_score
            team.save()
        game.status = Game.STATUS_COMPLETED
        game.save()

    def _start_turn(self, game: Game, player: Player, messages: List[str]):
        hand = Hand.init(player.hand)
        draw_pile = Stack.init(game.draw_pile)

        if draw_pile.count < 2:
            discard_pile = Stack.init(game.discard_pile)
            if discard_pile.count < 3:
                messages.append(f'not enough cards remaining; round over')
                return self._end_game(game)
            self._replenish_draw_pile(draw_pile, discard_pile)
            game.discard_pile = str(discard_pile)

        hand.put(draw_pile.draw(2))
        player.hand = str(hand)
        game.draw_pile = str(draw_pile)
        player.save()
        game.save()
        messages.append(f'{player.name} drew 2 cards')
    
    def _replenish_draw_pile(self, draw_pile: Stack, discard_pile: Stack):
        top_of_discard = discard_pile.draw(1)
        draw_pile.put(discard_pile.draw(discard_pile.count))
        draw_pile.shuffle()
        discard_pile.put(top_of_discard)

    def append_book(self, book: str, cards: str, player: Player) -> List[str]:
        if player.id != self._get_current_player(self._get_game()).id:
            raise InvalidActionError('it is not your turn')
        
        team = player.team
        team_books: Books = Books.init(team.books)
        try:
            book_to_append = team_books.get(book)
        except Exception:
            raise InvalidActionError(f'there is no book: {book}')

        try:
            cards_to_add = Stack.init(cards)
        except Exception:
            raise InvalidActionError(f'invalid cards: {cards}')

        hand = Hand.init(player.hand)
        self._take_cards(hand, cards_to_add)
        book_to_append.put(cards_to_add)

        self._validate_action(player.foot, hand, team_books)
        if not book_to_append.meta.is_valid:
            raise InvalidActionError(f'book is invalid: {str(book_to_append)}')

        player.hand = str(hand)
        team.books = str(team_books)
        team.save()
        player.save()

        messages = [f'{player.name} appended {str(cards_to_add)} to {book}']
        self._handle_foot(player, messages)

        return messages

    def create_books(self, books_str: str, player: Player) -> List[str]:
        if player.id != self._get_current_player(self._get_game()).id:
            raise InvalidActionError('it is not your turn')

        try:
            books = Books.init(books_str)
        except Exception:
            raise InvalidActionError(f'invalid cards {books_str}')

        team = player.team
        self._validate_books(books, self._get_game(), team)

        team_books = Books.init(team.books) + books
        hand = Hand.init(player.hand)
        for book in books.all:
            self._take_cards(hand, book)
        self._validate_action(player.foot, hand, team_books)

        player.hand = str(hand)
        team.books = str(team_books)
        team.save()
        player.save()
    
        messages = [f'{player.name} created new books: {str(books)}']
        self._handle_foot(player, messages)

        return messages
    
    def _take_cards(self, hand: Hand, cards: Stack) -> None:
        for card in cards:
            try:
                hand.take(card)
            except ValueError as error:
                raise InvalidActionError(str(error))
    
    def _validate_books(self, books: Books, game: Game, team: Team) -> None:
        for book in books.all:
            if not book.meta.is_valid:
                raise InvalidActionError(f'invalid book: {str(book)}')
        if team.books != '':
            # Team already has books, so no point threshold applies
            return
        min_points = REQ_POINTS_FOR_BOOK_CREATION[game.game_num % 6]
        points = books.sum_points()
        if points < min_points:
            raise InvalidActionError(f'need {min_points} points, found: {points}')


    def _validate_action(self, foot: str, hand: Hand, team_books: Books):
        if foot != '':
            # player still has a foot, so any hand size is valid
            return
        if hand.count < 1:
            raise InvalidActionError('hand would be empty')
        if hand.count == 1 and not team_books.can_end_game:
            raise InvalidActionError('not eligible to end game')

    def _handle_foot(self, player: Player, messages: List[str]):
        if player.foot == '' or player.hand != '':
            return
        player.hand = player.foot
        player.foot = ''
        player.save()
        messages.append(f'{player.name} has picked up their foot')
    
    def start_round(self) -> List[str]:
        game = self._get_game()
        self._check_can_start_round(game)
        game.game_num += 1
        game.status = Game.STATUS_IN_PROGRESS
        game.save()
    
        for team in game.teams.all():
            self._reset_books(team)

        deck = self._generate_game_deck(game)
        for player in game.players.all():
            self._deal_hand_and_foot(player, deck)
        game.discard_pile = str(Stack(deck.draw()))
        game.draw_pile = str(deck)
        game.save()

        # Start the first turn
        messages = ['The game has started!']
        self._start_turn(game, self._get_current_player(game), messages)

        return messages

    def _check_can_start_round(self, game: Game) -> None:
        if game.status == Game.STATUS_IN_PROGRESS:
            raise InvalidActionError('cannot start new round; round in progress')

        if game.status == Game.STATUS_COMPLETED:
            # This is always allowed, since pregame checks were run earlier
            return

        # Ensure all players are in a team
        for player in game.players.all():
            if player.team is None:
                raise InvalidActionError(f'cannot start game, player {player.name} is not in a team.')
        # Ensure all teams have at least one player
        for team in game.teams.all():
            if not team.players.all():
                raise InvalidActionError(f'cannot start game, team {team.name} has no players.')

    def _reset_books(self, team: Team) -> None:
        team.books = ''
        team.save()
    
    def _generate_game_deck(self, game: Game) -> Stack:
        number_of_players = len(game.players.all())
        stack = Stack.init(DECK * (number_of_players + 1))
        stack.shuffle()
        return stack
    
    def _deal_hand_and_foot(self, player: Player, deck: Stack) -> None:
        player.hand = str(Hand(deck.draw(11)))
        player.foot = str(Hand(deck.draw(11)))
        player.save()
