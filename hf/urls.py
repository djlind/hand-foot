from django.urls import path

from . import views

app_name = 'hf'
urlpatterns = [
    path('', views.index, name='index'),
    path('api/games/', views.apiGames, name='apiGames'),
    path('api/games/<slug:game_id>/', views.apiGetGame, name='apiGetGame'),
    path('api/games/<slug:game_id>/players/', views.apiPlayers, name='apiPlayers'),
    path('api/games/<slug:game_id>/sessionplayer/', views.apiGetSessionPlayer, name='apiGetSessionPlayer'),
    path('chat/<str:room_name>/', views.room, name='room'),
    path('chat/<str:room_name>/setName/', views.setName, name='setName'),
    path('debug/<str:room_name>/', views.debugSession, name='debugSession'),
    path('join/<slug:game_id>/', views.joinGame, name='joinGame'),
    path('join/<slug:game_id>/addPlayer/', views.addPlayer, name='addPlayer'),
    path('join/<slug:game_id>/joinTeam/<int:team_id>/', views.joinTeam, name='joinTeam'),
    path('join/<slug:game_id>/addTeam/', views.addTeam, name='addTeam'),
    path('games/<slug:game_id>/', views.gameDetailWS, name='gameDetailWS'),
    path('createGame/', views.createGame, name='createGame'),
    path('new/', views.gameCreate, name='gameCreate'),
    path('players/create/', views.playerCreate, name='playerCreate'),
]
