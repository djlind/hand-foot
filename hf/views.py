import json
from io import BytesIO

from django.http import HttpResponse, Http404, HttpResponseRedirect, HttpRequest, JsonResponse
from django.urls import reverse
from django.shortcuts import render
from django.utils.safestring import mark_safe

from .models import Game, Player, Team, Session

def apiGames(request: HttpRequest):
    if request.method == 'GET':
        return apiListGames()
    if request.method == 'POST':
        return apiCreateGame(request)
    raise ValueError('Unsupported HTTP method')

def apiListGames():
    game_list = Game.objects.all()
    return JsonResponse({'games': [game.to_json() for game in game_list]})

def apiCreateGame(request: HttpRequest):
    game = json.load(BytesIO(request.body))
    new_game = Game.objects.create(name=game.get('display_name', 'Untitled Game'))
    new_game.save()
    return JsonResponse(new_game.to_json())

def apiGetGame(request: HttpRequest, game_id: str):
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise ValueError('Game does not exist')
    return JsonResponse(game.to_json())

def apiPlayers(request: HttpRequest, game_id: str):
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise ValueError(f'Could not find game: {game_id}')

    if request.method == 'GET':
        return None
    if request.method == 'POST':
        return apiCreatePlayer(request, game)
    raise ValueError('Unsupported HTTP method')

def apiGetSessionPlayer(request: HttpRequest, game_id: str):
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise ValueError(f'Could not find game: {game_id}')

    if request.session.session_key is None:
        raise ValueError('No session cookie found')

    try:
        session_player = game.players.get(session__pk=request.session.session_key)
    except Player.DoesNotExist:
        raise Http404('Current session is not associated with a player')

    return JsonResponse(session_player.to_json())

def apiCreatePlayer(request: HttpRequest, game: Game):
    if request.session.session_key is None:
        print('creating new session')
        request.session.cycle_key()
    # Get the session object
    try:
        session = Session.objects.get(pk=request.session.session_key)
    except Session.DoesNotExist:
        raise ValueError(f"Session: {request.session.session_key} does not exist")

    if len(game.players.filter(session__pk=request.session.session_key)) > 0:
        raise ValueError("Cannot have more than one player per session")

    player = json.load(BytesIO(request.body))
    new_player = Player.objects.create(session=session, game=game, name=player.get('display_name'))

    return JsonResponse(new_player.to_json())

def index(request):
    game_list = Game.objects.all()
    context = {'game_list': game_list}
    return render(request, 'hf/index.html', context)

def room(request, room_name):
    if not request.session.get('name', False):
        return render(request, 'hf/setname.html', {'room_name': room_name})
    return render(request, 'hf/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    })


def setName(request, room_name):
    name = request.POST['new-session-name']
    request.session['name'] = name
    return HttpResponseRedirect(reverse('hf:room', args=(room_name,)))


def debugSession(request, room_name):
    return HttpResponse(f'Your session name is: {request.session.get("name", "undefined")}')


def addPlayer(request, game_id):
    # Check we have a valid game id
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise Http404(f"Game: {game_id} does not exist")

    # Add the user's IGN to the session
    request.session['name'] = request.POST['username']
    if request.session.session_key is None:
        request.session.cycle_key()

    # Get the session object
    try:
        session = Session.objects.get(pk=request.session.session_key)
    except Session.DoesNotExist:
        raise Http404(f"Session: {request.session.session_key} does not exist")

    # Create a player in this game if session is not connected to one already
    if len(game.players.filter(session__pk=request.session.session_key)) == 0:
        Player.objects.create(session=session, game=game, name=request.POST['username'])

    return HttpResponseRedirect(reverse('hf:gameDetailWS', args=(game_id,)))


def joinGame(request, game_id):
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise Http404(f"Game: {game_id} does not exist")
    return render(request, 'hf/joinGame.html', {'game': game})


def gameDetail(request, game_id):
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise Http404("Game with id: {game_id} does not exist")
    return render(request, 'hf/gameDetail.html', {'game': game})


def gameCreate(request):
    return render(request, 'hf/gameCreate.html')


def playerCreate(request):
    return HttpResponse("This will be the player creation view.")


def joinTeam(request, game_id, team_id):
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise Http404("Game with id: {game_id} does not exist")

    try:
        team = game.teams.get(id=team_id)
    except Team.DoesNotExist:
        raise Http404("Team with id: {team_id} does not exist")

    try:
        player = game.players.get(session__pk=request.session.session_key)
    except Player.DoesNotExist:
        raise Http404(f"Player with session id: {request.session.session_key} does not exist")

    # Assign player to team
    player.team = team
    player.save()

    return HttpResponseRedirect(reverse('hf:gameDetail', args=(game_id,)))


def addTeam(request, game_id):
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise Http404(f"Game with id: {game_id} does not exist")

    # Create empty team
    team = game.teams.create(name=request.POST['teamname'])

    return joinTeam(request, game_id, team.id)


def gameDetailWS(request, game_id):
    # Check we have a valid game id
    try:
        game = Game.objects.get(id=game_id)
    except Game.DoesNotExist:
        raise Http404("Game with id: {game_id} does not exist")

    # Check user has a name
    try:
        player = game.players.get(session__pk=request.session.session_key)
    except Player.DoesNotExist:
        return HttpResponseRedirect(reverse('hf:joinGame', args=(game_id,)))

    return render(request, 'hf/gameDetailWS.html', {
        'room_name_json': mark_safe(json.dumps(game_id)),
        'game': game,
    })


def createGame(request: HttpRequest):
    name = request.POST.get('new-game-name', '')
    new_game = Game.objects.create(name=name)
    new_game.save()
    return joinGame(request, new_game.id)
