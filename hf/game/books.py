from collections import defaultdict
from typing import List

from .book import Book
from .factory import factory


class Books:
    SEPARATOR = '|'
    all: List[Book]

    def __init__(self, list_of_books: List[Book]):
        self.all = list_of_books

    def __add__(self, rh):
        assert type(rh) == Books
        return Books(self.all + rh.all)

    @classmethod
    def from_string(cls, serialized_books: str):
        if serialized_books == '':
            return cls([])
        return cls([Book.from_string(book) for book in serialized_books.split(cls.SEPARATOR)])

    init = classmethod(factory)

    def __repr__(self):
        return self.SEPARATOR.join([str(book) for book in self.all])

    def to_dict(self):
        result = defaultdict(list)
        for book in self.all:
            result[book.meta.rank.value].append([str(book), book.meta.is_clean, book.meta.is_complete])

        return result
    
    def get(self, serialized_book: str):
        for book in self.all:
            if str(book) == serialized_book:
                return book
        raise ValueError('book not found')

    @property
    def can_end_game(self):
        clean_count = 0
        dirty_count = 0
        for book in self.all:
            if book.meta.is_complete:
                if book.meta.is_clean:
                    clean_count += 1
                else:
                    dirty_count += 1

        return clean_count >= 2 and dirty_count >= 3

    def sum_points(self):
        total_points = 0
        for book in self.all:
            total_points += book.meta.points
        return total_points
