def factory(cls, input_obj):
    obj_type = type(input_obj)
    if obj_type == str:
        return cls.from_string(input_obj)
    elif obj_type == cls:
        return input_obj
    else:
        raise ValueError(f'Cannot init {cls} from {obj_type}')
