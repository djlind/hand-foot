from enum import Enum


class Suite(Enum):
    HEART = 'H'
    DIAMOND = 'D'
    CLUB = 'C'
    SPADE = 'S'
    JOKER = 'J'
