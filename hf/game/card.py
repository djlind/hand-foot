from .rank import Rank
from .suite import Suite

CARD_SCORES = {
    Rank.JOKER: 50,
    Rank.ACE: 20,
    Rank.TWO: 20,
    Rank.THREE: 5,
    Rank.FOUR: 5,
    Rank.FIVE: 5,
    Rank.SIX: 5,
    Rank.SEVEN: 5,
    Rank.EIGHT: 10,
    Rank.NINE: 10,
    Rank.TEN: 10,
    Rank.JACK: 10,
    Rank.QUEEN: 10,
    Rank.KING: 10
}


class Card:
    def __init__(self, serialized: str):
        self.rank = Rank(serialized[0])
        self.suite = Suite(serialized[1])
        self._serialized = serialized

    def __repr__(self):
        return f"{self.rank.value}{self.suite.value}"

    def __eq__(self, rh):
        assert type(rh) == Card
        return self.rank == rh.rank and self.suite == rh.suite

    @property
    def score(self):
        if self.rank == Rank.THREE and self.suite in (Suite.DIAMOND, Suite.HEART):
            return 200
        else:
            return CARD_SCORES[self.rank]

    @property
    def is_wild(self):
        return (self.rank == Rank.JOKER or self.rank == Rank.TWO)

    @property
    def is_natural(self):
        return not self.is_wild

    def to_string(self):
        return self._serialized
