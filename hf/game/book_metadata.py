from .rank import Rank


class BookMetadata():
    def __init__(self, cards: list, checksum: str):
        self.checksum = checksum
        self.count_natural = 0
        self.rank = None
        self.points = 0
        for card in cards:
            self.points += card.score
            if card.is_wild:
                continue
            if self.rank is None:
                self.rank = card.rank
            if card.rank != self.rank:
                self.rank = Rank.INVALID
            self.count_natural += 1
        self.count_wild = len(cards) - self.count_natural
        self.is_valid = (
            len(cards) >= 3
            and self.rank is not None
            and self.rank != Rank.INVALID
            and self.rank != Rank.THREE
            and self.count_natural > self.count_wild
        )
        self.is_complete = len(cards) >= 7
        self.is_dirty = self.count_wild > 0
        self.is_clean = not self.is_dirty
        if self.is_complete:
            if self.is_clean:
                self.points += 500
            elif self.is_dirty:
                self.points += 300
