from .stack import Stack


class Hand(Stack):
    def take(self, card):
        if not self.contains(card):
            raise ValueError("Hand does not contain " + str(card))
        self._cards.remove(card)
