from .stack import Stack
from .book_metadata import BookMetadata


class Book(Stack):
    @property
    def meta(self):
        try:
            cache = self._meta
            if cache.checksum != str(self):
                raise AttributeError('Invalid cache')
        except AttributeError:
            self._meta = BookMetadata(self._cards, str(self))

        return self._meta

    def __eq__(self, rh):
        return self._cards == rh._cards

    def get_cards(self):
        return self._cards
