from random import shuffle
from typing import List

from .card import Card
from .factory import factory


class Stack:
    _cards: List[Card]

    def __init__(self, cards: list = None):
        if cards is None:
            cards = []
        self._cards = cards

    def __repr__(self):
        return ''.join([str(card) for card in self._cards])

    @classmethod
    def from_string(cls, input_cards: str):
        serial_cards = input_cards.upper()
        assert len(serial_cards) % 2 == 0
        return cls([Card(serial_cards[idx:idx+2]) for idx in range(0, len(serial_cards), 2)])

    init = classmethod(factory)

    @property
    def count(self) -> int:
        return len(self._cards)

    def shuffle(self):
        if (self.count > 0):
            shuffle(self._cards)
        else:
            raise ValueError("Cannot shuffle an empty stack of cards")

    def draw(self, nCards: int = 1) -> list:
        if (self.count >= nCards):
            return [self._cards.pop() for _ in range(0, nCards)]
        else:
            raise ValueError("Cannot draw from an empty stack of cards")

    def peek(self):
        if (self.count > 0):
            return self._cards[-1]
        else:
            raise ValueError("Cannot peek an empty stack of cards")

    def contains(self, card: Card):
        return card in self._cards

    def put(self, cards):
        for card in cards:
            self._cards.append(card)

    def __iter__(self):
        return (card for card in self._cards)
