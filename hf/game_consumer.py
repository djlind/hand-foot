import json
from dataclasses import asdict
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from typing import List, Callable

from .game_api import EndgameState, GameApi, InvalidActionError
from .models import Game, Player


class GameDetailConsumer(WebsocketConsumer):
    room_group_name: str
    session_key: str
    api: GameApi

    @property
    def session_player(self) -> Player:
        return self.api.get_player(self.session_key)

    def connect(self):
        game_id = self.scope['url_route']['kwargs']['game_id']
        self.room_group_name = f'chat_{game_id}'
        self.session_key = self.scope['session'].session_key
        self.api = GameApi(game_id=game_id)

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        self.broadcast_system_message(f'{self.session_player.name} has disconnected')
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        # Receive message from WebSocket
        text_data_json = json.loads(text_data)
        dtype = text_data_json['type']
        if dtype == 'discard':
            self.handle_errors(lambda: self.discard(text_data_json))
        elif dtype == 'append_book':
            self.handle_errors(lambda: self.append_book(text_data_json))
        elif dtype == 'create_books':
            self.handle_errors(lambda: self.create_books(text_data_json))
        elif dtype == 'new_round' or dtype == 'start_game':
            self.handle_errors(lambda: self.start_round())
        elif dtype == 'create_team':
            self.handle_errors(lambda: self.create_team(text_data_json))
        elif dtype == 'join_team':
            self.handle_errors(lambda: self.join_team(text_data_json))
        elif dtype == 'chat_message':
            self.receive_chat_message(text_data_json)
        elif dtype == 'open_connection':
            self.open_connection()
        else:
            self.send_error('received unknown socket message type')
    
    def discard(self, event: dict):
        messages = self.api.discard(event['discard_card'], self.session_player)
        self.broadcast_system_messages(messages)
        if self.api.get_game_status() == Game.STATUS_COMPLETED:
            self.broadcast_game_end()
        else:
            self.broadcast_game_update()
    
    def append_book(self, event: dict):
        messages = self.api.append_book(event['book'], event['cards'], self.session_player)
        self.broadcast_system_messages(messages)
        self.broadcast_game_update()
    
    def create_books(self, event: dict):
        messages = self.api.create_books(event['books'], self.session_player)
        self.broadcast_system_messages(messages)
        self.broadcast_game_update()

    def start_round(self):
        messages = self.api.start_round()
        self.broadcast_system_messages(messages)
        self.broadcast_game_update()

    def create_team(self, event: dict):
        self.api.create_team(event['name'], self.session_player)
        self.broadcast_pregame_update()

    def join_team(self, event: dict):
        self.api.join_team(event['team_id'], self.session_player)
        self.broadcast_pregame_update()

    def open_connection(self):
        status = self.api.get_game_status()
        self.broadcast_system_message(f'{self.session_player.name} has reconnected')
        if status == Game.STATUS_AWAITING:
            self.broadcast_pregame_update()
        elif status == Game.STATUS_IN_PROGRESS:
            self.game_update(self.build_game_update())
        elif status == Game.STATUS_COMPLETED:
            self.game_end(self.build_scoring())

    def receive_chat_message(self, event: dict):
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': event['message'],
                'name': self.session_player.name,
                'session_key': self.session_key,
            }
        )

    def handle_errors(self, func: Callable):
        try:
            return func()
        except ValueError as e:
            self.send_error(str(e))
        except InvalidActionError as e:
            self.send_error(str(e))

    def send_error(self, detail):
        print('[Debug] Sending socket error:', detail)
        self.send(text_data=json.dumps({
            'type': 'error',
            'detail': detail,
        }))

    def broadcast_system_messages(self, updates: List[str]):
        for update in updates:
            self.broadcast_system_message(update)

    def broadcast_system_message(self, str_update):
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_update',
                'message': f'**{str_update}**',
            }
        )

    def broadcast_pregame_update(self):
        state = self.api.get_pregame_state()
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'game_details',
                'teams': [asdict(team) for team in state.teams],
                'free_players': state.free_players
            }
        )

    def broadcast_game_update(self):
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            self.build_game_update()
        )
    
    def broadcast_game_end(self):
        # TODO: Move calculating winner to API
        scoring = self.build_scoring()
        round_winner = scoring['teams'][0]
        total_winner = scoring['teams'][0]
        for team in scoring['teams']:
            if team['round_score'] > round_winner['round_score']:
                round_winner = team
            if team['score'] > total_winner['score']:
                total_winner = team

        self.broadcast_system_message(
            f'{round_winner["name"]} won the round with {round_winner["round_score"]} points')
        self.broadcast_system_message(
            f'{total_winner["name"]} is winning overall with {total_winner["score"]} cumulative '
            'points')
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            scoring,
        )

    def build_game_update(self):
        state = self.api.get_game_state()
        return {
            'type': 'game_update',
            'discard_top': state.discard_top,
            'draw_pile_len': state.draw_pile_len,
            'teams': [team.to_dict() for team in state.teams],
            'current_turn': state.current_turn,
        }

    def build_scoring(self):
        state = self.api.get_endgame_state()
        return {
            'type': 'game_end',
            'teams': self.reformat_endgame_state(state)
        }
    
    def reformat_endgame_state(self, state: EndgameState):
        output = []
        scores = {team.id: team for team in state.team_scores}
        for team in state.final_state.teams:
            team_score = scores[team.id]
            player_scores = {player.id: player for player in team_score.player_scores}
            team_state = team.to_dict()
            team_state['round_score'] = team_score.round_score
            for player in team_state['players']:
                player['scoring'] = asdict(player_scores[player['id']])
            output.append(team_state)

        return output

    def chat_message(self, event):
        message = event['message']
        message_name = event['name']
        session_id = event['session_key']

        if self.session_key == session_id:
            message_name = 'you'
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'type': 'chat_message',
            'message': message_name + ': ' + message
        }))

    def chat_update(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))

    def game_details(self, event):
        """Forward event information through the websocket"""
        self.send(text_data=json.dumps(event))

    def game_update(self, event):
        player_details = self.api.get_player_detail(self.session_player)
        event['player'] = {
            'hand': str(player_details.hand),
            'is_turn': player_details.is_turn,
            'team_id': player_details.team_id,
        }
        self.send(text_data=json.dumps(event))

    def game_end(self, event):
        player_details = self.api.get_player_detail(self.session_player)
        event['player'] = {
            'hand': str(player_details.hand),
            'is_turn': False,
            'team_id': player_details.team_id,
        }
        self.send(text_data=json.dumps(event))
