FROM python:3.8

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

# create root directory for our project in the container
RUN mkdir /hand-foot

# Set the working directory to /hand-foot
WORKDIR /hand-foot

COPY requirements.txt /hand-foot/requirements.txt

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Copy the current directory contents into the container at /hand-foot
COPY . /hand-foot/

# Declare that we are going to listen on port 8001
EXPOSE 8001

# Point Django to settings file
ENV DJANGO_SETTINGS_MODULE=v2.settings

# Run migrations
RUN python -m django migrate

# Collect static files (is this necessary?)
RUN python -m django collectstatic

# Run the server using daphne
ENTRYPOINT [ "daphne", "-b", "0.0.0.0", "-p", "8001", "v2.asgi:application" ]
