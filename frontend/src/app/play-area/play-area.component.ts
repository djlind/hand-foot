import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AppendBookRequest, GameServerMessage } from '../game/socket-types';
import { Team } from '../game/team';

@Component({
  selector: 'app-play-area',
  templateUrl: './play-area.component.html',
  styleUrls: ['./play-area.component.scss']
})
export class PlayAreaComponent {
  @Input()
  set message(message: GameServerMessage) {
    this.playerTeam = this.getPlayerTeam(message);
    this.otherTeams = message.teams
      .map((team) => Team.create(team))
      .filter((team) => team.id != this.playerTeam?.id);
    this.isTurn = message.player.is_turn;
    this.hand = this.getHand(message.player.hand);
  }

  @Output() discard: EventEmitter<string> = new EventEmitter();
  @Output() createBooks: EventEmitter<string> = new EventEmitter();
  @Output() appendBook: EventEmitter<AppendBookRequest> = new EventEmitter();

  createBooksInput: string = '';
  playerTeam?: Team;
  otherTeams: Team[] = [];
  isTurn: boolean = false;
  hand: string[] = [];

  private getPlayerTeam(message: GameServerMessage): Team | undefined {
    for (let team of message.teams) {
      if (team.id == message.player.team_id) {
        return Team.create(team);
      }
    }
    return undefined;
  }

  private getHand(hand: string): string[] {
    let cards = [];
    for (let i = 0; i < hand.length; i += 2) {
      cards.push(hand[i] + hand[i + 1]);
    }
    cards.sort();
    return cards;
  }

  createBooksHandler() {
    this.createBooks.emit(this.createBooksInput);
    this.createBooksInput = '';
  }
}
