import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Game } from '../game';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent {
  games?: Game[];
  newGameDisplayName: string = '';
  
  constructor(private gameService: GameService, private router: Router) {}

  ngOnInit() {
    this.loadGames();
  }

  loadGames() {
    this.gameService.listGames().subscribe(result => this.games = result.games);
  }

  createNewGame() {
    let game = {'display_name': this.newGameDisplayName};
    this.gameService.createGame(game).subscribe(response => this.router.navigateByUrl("join/" + response.name));
  }

}
