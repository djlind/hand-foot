import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PregameServerMessage } from '../game/socket-types';

@Component({
  selector: 'app-pregame',
  templateUrl: './pregame.component.html',
  styleUrls: ['./pregame.component.scss']
})
export class PregameComponent {
  @Input() message?: PregameServerMessage;
  @Output() createTeam: EventEmitter<string> = new EventEmitter();
  @Output() joinTeam: EventEmitter<number> = new EventEmitter();
  @Output() startGame: EventEmitter<null> = new EventEmitter();

  createTeamInput: string = '';

  clickCreateTeam() {
    this.createTeam.emit(this.createTeamInput);
    this.createTeamInput = '';
  }

  clickJoinTeam(teamId: number) {
    this.joinTeam.emit(teamId);
  }

  clickStartGame() {
    this.startGame.emit();
  }
}
