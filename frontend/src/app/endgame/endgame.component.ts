import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EndgameServerMessage, TeamDetailWithScore } from '../game/socket-types';

@Component({
  selector: 'app-endgame',
  templateUrl: './endgame.component.html',
  styleUrls: ['./endgame.component.scss']
})
export class EndgameComponent {
  @Output() newRound: EventEmitter<null> = new EventEmitter();

  @Input()
  public set message(msg: EndgameServerMessage) {
    let teams = [...msg.teams];
    this.sortedTeams = teams.sort((a, b) => a.round_score - b.round_score);
  }
  

  sortedTeams: TeamDetailWithScore[] = [];


}
