import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Game, ListGamesResponse, Player } from './game';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  API_BASE_URL = "http://localhost/hf/api/";
  private readonly httpOptions = {'withCredentials': true};

  constructor(private http: HttpClient) { }

  listGames(): Observable<ListGamesResponse> {
    return this.http.get(this.API_BASE_URL + "games/", this.httpOptions) as Observable<ListGamesResponse>;
  }

  createGame(game: Game): Observable<Game> {
    return this.http.post(this.API_BASE_URL + "games/", game, this.httpOptions) as Observable<Game>;
  }

  getGame(name: string): Observable<Game> {
    return this.http.get(this.API_BASE_URL + name + '/', this.httpOptions) as Observable<Game>;
  }

  createPlayer(parent: string, player: Player): Observable<Player> {
    return this.http.post(this.API_BASE_URL + parent + '/players/', player, this.httpOptions) as Observable<Player>;
  }

  getSessionPlayer(parent: string): Observable<Player> {
    return this.http.get(this.API_BASE_URL + parent + '/sessionplayer/', this.httpOptions) as Observable<Player>;
  }
}
