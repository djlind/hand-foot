export interface Game {
  status?: number,
  name?: string,
  draw_pile?: string,
  discard_pile?: string,
  current_turn?: number,
  game_num?: number,
  display_name?: string,
}

export interface Player {
  name?: string,
  display_name?: string,
  hand?: string,
  foot?: boolean,
  team?: string,
}

export interface ListGamesResponse {
  games: Game[],
}

export interface JoinGameRequest {
  
}
