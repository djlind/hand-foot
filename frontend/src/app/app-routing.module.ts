import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameListComponent } from './game-list/game-list.component';
import { GameComponent } from './game/game.component';
import { JoinGameComponent } from './join-game/join-game.component';

const routes: Routes = [
    {path: "", component: GameListComponent},
    {path: "join/games/:gameId", component: JoinGameComponent},
    {path: "play/games/:gameId", component: GameComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
