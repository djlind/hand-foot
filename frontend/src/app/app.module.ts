import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameListComponent } from './game-list/game-list.component';
import { FormsModule } from '@angular/forms';
import { JoinGameComponent } from './join-game/join-game.component';
import { GameComponent } from './game/game.component';
import { BookComponent } from './book/book.component';
import { TeamComponent } from './team/team.component';
import { EndgameComponent } from './endgame/endgame.component';
import { PregameComponent } from './pregame/pregame.component';
import { HandComponent } from './hand/hand.component';
import { PlayAreaComponent } from './play-area/play-area.component';

@NgModule({
  declarations: [
    AppComponent,
    GameListComponent,
    JoinGameComponent,
    GameComponent,
    BookComponent,
    TeamComponent,
    EndgameComponent,
    PregameComponent,
    HandComponent,
    PlayAreaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
