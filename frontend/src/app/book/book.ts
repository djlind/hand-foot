export interface Book {
    rank: string
    book: string
    isClean: boolean
    isComplete: boolean
};
