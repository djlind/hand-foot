import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Book } from './book';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent {
  @Input() book?: Book;
  @Input() showAppend?: boolean;
  @Output() appendEvent: EventEmitter<string> = new EventEmitter();

  appendBookInput: string = '';

  appendBook() {
    this.appendEvent.emit(this.appendBookInput);
    this.appendBookInput = '';
  }
}
