import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AppendBookRequest } from '../game/socket-types';
import { Team } from '../game/team';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent {
  @Input() team?: Team;
  @Input() showControls: boolean = false;
  @Output() appendBookEvent: EventEmitter<AppendBookRequest> = new EventEmitter();

  appendEvent(book: string, cards: string) {
    this.appendBookEvent.emit({type: 'append_book', book, cards});
  }
}
