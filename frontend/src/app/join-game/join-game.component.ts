import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GameService } from '../game.service';

@Component({
  selector: 'app-join-game',
  templateUrl: './join-game.component.html',
  styleUrls: ['./join-game.component.scss']
})
export class JoinGameComponent {
  gameName?: string;
  userName: string = '';

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.gameName = `games/${params['gameId']}`;
      this.gameService.getSessionPlayer(this.gameName).subscribe(
        player => this.router.navigateByUrl(`play/${this.gameName}`)
      );
    })
  }

  submit() {
    let parent = this.gameName;
    if (!parent) return;
    this.gameService.createPlayer(parent, {'display_name': this.userName}).subscribe(
      player => this.router.navigateByUrl(`play/${this.gameName}`)
    );
  }
}
