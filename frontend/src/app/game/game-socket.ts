import { Observable, Subject } from "rxjs";
import { webSocket, WebSocketSubject } from "rxjs/webSocket"
import { Message, PregameServerMessage, GameServerMessage, EndgameServerMessage } from "./socket-types";

export class GameSocket {

  WEBSOCKET_BASE_URL = 'ws://localhost/ws/hf/'
  private socket: WebSocketSubject<Message>;
  private chatSubject: Subject<string> = new Subject();
  private pregameSubject: Subject<PregameServerMessage> = new Subject();
  private gameSubject: Subject<GameServerMessage> = new Subject();
  private endgameSubject: Subject<EndgameServerMessage> = new Subject();

  constructor(gameName: string) {
    this.socket = webSocket(this.WEBSOCKET_BASE_URL + gameName + '/');
    this.socket.asObservable().subscribe(data => this.receiveData(data));
  }

  private receiveData(data: any) {
    console.log(data);
    if (!data) {
      return;
    }
    switch (data['type']) {
      case 'chat_update':
      case 'chat_message':
        this.chatSubject.next(data['message']);
        break;
      case 'game_details':
        this.pregameSubject.next(data);
        break;
      case 'game_update':
        this.gameSubject.next(data);
        break;
      case 'game_end':
        this.gameSubject.next(data);
        this.endgameSubject.next(data);
        break;
    }
  }

  getChatMessages(): Observable<string> {
    return this.chatSubject.asObservable();
  }

  sendChatMessage(message: string) {
    this.socket.next({'type': 'chat_message', message});
  }

  getPregameMessages(): Observable<PregameServerMessage> {
    return this.pregameSubject.asObservable();
  }

  getGameMessages(): Observable<GameServerMessage> {
    return this.gameSubject.asObservable();
  }

  getEndgameMessages(): Observable<EndgameServerMessage> {
    return this.endgameSubject.asObservable();
  }

  createTeam(teamName: string) {
    this.socket.next({'type': 'create_team', 'name': teamName});
  }

  joinTeam(teamId: number) {
    this.socket.next({'type': 'join_team', 'team_id': teamId});
  }

  openConnection() {
    this.socket.next({'type': 'open_connection'});
  }

  startGame() {
    this.socket.next({'type': 'start_game'});
  }

  discard(card: string) {
    this.socket.next({'type': 'discard', 'discard_card': card});
  }

  createBooks(books: string) {
    this.socket.next({'type': 'create_books', books});
  }

  appendBook(book: string, cards: string) {
    this.socket.next({'type': 'append_book', book, cards});
  }

  startNewRound() {
    this.socket.next({'type': 'new_round'});
  }
}
