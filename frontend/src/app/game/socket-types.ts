export type Message = ChatServerMessage | PregameServerMessage | GameServerMessage |
  EndgameServerMessage | CreateTeamRequest | JoinTeamRequest | OpenConnectionRequest |
  StartGameRequest | DiscardRequest | CreateBooksRequest | AppendBookRequest |
  NewRoundRequest;

interface ChatServerMessage {
  type: 'chat_message'
  message: string
}

export interface PregameServerMessage {
  type: 'game_details'
  teams: Team[]
  free_players: string[]
}

export interface GameServerMessage {
  type: 'game_update'
  discard_top: string
  draw_pile_len: number
  teams: TeamDetail[]
  current_turn: number
  player: {hand: string, is_turn: boolean, team_id: number}
}

export interface EndgameServerMessage {
  type: 'game_end'
  teams: TeamDetailWithScore[]
}

interface Team {
  id: number
  name: string
  players: string[]
}

export interface TeamDetail {
  id: number
  name: string
  players: PlayerDetail[]
  books: Books
  score: number
  current_score: number
}

export interface TeamDetailWithScore extends TeamDetail {
  players: PlayerDetailWithScore[]
  round_score: number
}

export interface PlayerDetail {
  id: number
  name: string
  has_foot: boolean
  hand_len: number
}

export interface PlayerDetailWithScore extends PlayerDetail {
  scoring: {hand: number, foot: number}
}

type Book = [string, boolean, boolean];

interface Books {
  [key: string]: Book[]
}

interface CreateTeamRequest {
  type: 'create_team'
  name: string
}

interface JoinTeamRequest {
  type: 'join_team'
  team_id: number
}

interface OpenConnectionRequest {
  type: 'open_connection'
}

interface StartGameRequest {
  type: 'start_game'
}

interface DiscardRequest {
  type: 'discard'
  discard_card: string
}

interface CreateBooksRequest {
  type: 'create_books'
  books: string
}

export interface AppendBookRequest {
  type: 'append_book'
  book: string
  cards: string
}

interface NewRoundRequest {
  type: 'new_round'
}
