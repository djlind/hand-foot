import { Book } from '../book/book';
import { PlayerDetail, TeamDetail } from './socket-types';

export class Team {
  constructor(
    public id: number,
    public name: string,
    public books: Book[],
    public players: PlayerDetail[]
  ) { }

  static create(teamDetail: TeamDetail): Team {
    let books = [];
    for (let rank of Object.keys(teamDetail.books)) {
      let [book, isClean, isComplete] = teamDetail.books[rank][0];
      books.push({rank, book, isClean, isComplete});
    }
    return new Team(teamDetail.id, teamDetail.name, books, teamDetail.players);
  }
}
