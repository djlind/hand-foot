import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Game } from '../game';
import { GameService } from '../game.service';
import { GameSocket } from './game-socket';
import { GameServerMessage, PregameServerMessage, EndgameServerMessage } from './socket-types';


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent {
  game?: Game;
  gameSocket?: GameSocket;

  chatInput: string = '';
  chatLog: string = '';

  latestPregameMessage?: PregameServerMessage;
  latestGameMessage?: GameServerMessage;
  endGameMessage?: EndgameServerMessage;

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.gameService.getGame(`games/${params['gameId']}`).subscribe(game => this.game = game);
      this.gameSocket = new GameSocket(`games/${params['gameId']}`);
      this.gameSocket?.getChatMessages().subscribe(msg => this.receiveChat(msg));
      this.gameSocket?.getPregameMessages().subscribe(msg => this.latestPregameMessage = msg);
      this.gameSocket?.getGameMessages().subscribe(msg => this.latestGameMessage = msg);
      this.gameSocket?.getEndgameMessages().subscribe(msg => this.endGameMessage = msg);
      this.gameSocket?.openConnection();
    });
  }

  receiveChat(message: string) {
    this.chatLog += `${message}\n`;
  }

  sendChat() {
    this.gameSocket?.sendChatMessage(this.chatInput);
    this.chatInput = '';
  }

  startNewRound() {
    delete this.endGameMessage;
    this.gameSocket?.startNewRound();
  }
}
