import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-hand',
  templateUrl: './hand.component.html',
  styleUrls: ['./hand.component.scss']
})
export class HandComponent {
  @Input() showControls: boolean = false;
  @Input() cards: string[] = [];
  @Output() discard: EventEmitter<string> = new EventEmitter();
}
